#!/usr/bin/env bash

# This shell script is for initializing a web front-end development folder.
# It will generate several files by using commands below.

# First, check if ths folder is empty
if [ "$(ls -A)" ]; then
    read -r -p "The folder is not empty. Some files will be covered if continue. Continue? [y/N] " response
    if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]; then
        continue
    else
        exit
    fi
else
    continue
fi

# define variables
target_url="https://github.com/Joseph7451797/"
repo_name="webpack-reload-test"
target_branch="master"

say 前往tmp資料夾
cd /tmp
say 開始c,u,r,l
curl -L ${target_url}${repo_name}/archive/${target_branch}.tar.gz | tar xz
say 回到原目錄
cd -
say 複製檔案至本資料夾
cp -a /tmp/${repo_name}-${target_branch}/. ${PWD}

say 建立js資料夾
mkdir -v js
say 建立build資料夾
mkdir -v build

# cleanup README.md
echo '' > README.md
