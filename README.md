# 用 shell script 生成前端初始開發用資料夾 #

### Auto generation script ###
For auto generation, follow the step below：

- Go to your new project folder : ```$ cd path_to_your_new_project_folder/```
- Get the generation script using cURL : ```$ curl https://bitbucket.org/joseph_chang_/shellmakeproject/raw/1d49d4f0fafc4a17becadfdb88a9a208c7e4c7d6/make.sh | bash```

### Issue ###
This shell script only tested on Mac OSX. If you have any issue, plaese open issue on this [page](https://bitbucket.org/joseph_chang_/shellmakeproject/issues).

### License ###
MIT license
